// Using the library: https://github.com/MartyMacGyver/ESP32-Digital-RGB-LED-Drivers

#include "esp32_digital_led_lib.h"
#include "esp32_digital_led_funcs.h"

// These are taken from the library's examples.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"  // It's noisy here with `-Wall`
#pragma GCC diagnostic pop

/*
    Configuration
*/

strand_t STRAND = {.rmtChannel = 0, .gpioNum = 13, .ledType = LED_SK6812_V1, .brightLimit = 32, .numPixels =  150};

// LUT from (Y, X) (code formatted as you look at it) to position in the string.
// Two LEDs are hidden, disrupting the predictable rows and col assignments.
const int POSITION_LUT[9][16] = {
  //                    45                                       118
  {8,  9, 26, 27, 44, /*  */ 46, 63, 64, 81, 82, 99, 100, 117, /*   */ 119, 136, 137},
  {7, 10, 25, 28, 43, /*  */ 47, 62, 65, 80, 83, 98, 101, 116, /*   */ 120, 135, 138},
  {6, 11, 24, 29, 42, /*  */ 48, 61, 66, 79, 84, 97, 102, 115, /*   */ 121, 134, 139},
  {5, 12, 23, 30, 41, /*  */ 49, 60, 67, 78, 85, 96, 103, 114, /*   */ 122, 133, 140},
  {4, 13, 22, 31, 40, /*  */ 50, 59, 68, 77, 86, 95, 104, 113, /*   */ 123, 132, 141},
  {3, 14, 21, 32, 39, /*  */ 51, 58, 69, 76, 87, 94, 105, 112, /*   */ 124, 131, 142},
  {2, 15, 20, 33, 38, /*  */ 52, 57, 70, 75, 88, 93, 106, 111, /*   */ 125, 130, 143},
  {1, 16, 19, 34, 37, /*  */ 53, 56, 71, 74, 89, 92, 107, 110, /*   */ 126, 129, 144},
  {0, 17, 18, 35, 36, /*  */ 54, 55, 72, 73, 90, 91, 108, 109, /*   */ 127, 128, 145} // 146 147 148 149
};

const int MAX_X = 16;
const int MAX_Y = 9;

// This many in flight at once.
const int MAX_SNOWFLAKES = 10;

const float MIN_Y_VELOCITY = 0.005;
const float MAX_Y_VELOCITY = 0.1;

const float MIN_X_VELOCITY = 0.1;
const float MAX_X_VELOCITY = 0.4;


// Adjust down for testing.
const float MAX_INTENSITY = 255.0;

// Window size (+ and - each side of the float coordinate's floor) for downsampling.
// e.g. a value of 3 means 7 x 7 pixels are calculated. The size of this
// doesn't affect the calculation, only how many pixels it is calculated for.
const int DOWNSAMPLE_WINDOW = 5;

class Picture {
  public:

    // Three buffers for Red, Green, Blue.
    // For WWA these are Amber, Cold, Warm.
    float rBuf[MAX_X][MAX_Y];
    float gBuf[MAX_X][MAX_Y];
    float bBuf[MAX_X][MAX_Y];

    // virtual rainbow layer
    float wBuf[MAX_X][MAX_Y];

    void init() {
      for (int x = 0; x < MAX_X; x++) {
        for (int y = 0; y < MAX_Y; y++) {
          rBuf[x][y] = 0;
          gBuf[x][y] = 0;
          bBuf[x][y] = 0;
          wBuf[x][y] = 0;
        }
      }
    }

    // Blend and saturate pixel.
    void add(int x, int y, float r, float g, float b, float w) {
      //Serial.printf("Add (%d, %d), (%f, %f, %f)\n", x, y, rBrightness, gBrightness, bBrightness);

      if (x > MAX_X) {
        return;
      }

      if (y > MAX_Y) {
        return;
      }

      // Ignore sub-zero.
      rBuf[x][y] += max(r, 0.0f);
      gBuf[x][y] += max(g, 0.0f);
      bBuf[x][y] += max(b, 0.0f);
      wBuf[x][y] += max(w, 0.0f);


      rBuf[x][y] = min(rBuf[x][y], 1.0f);
      gBuf[x][y] = min(gBuf[x][y], 1.0f);
      bBuf[x][y] = min(bBuf[x][y], 1.0f);
      wBuf[x][y] = min(wBuf[x][y], 1.0f);
    }

    // Add XY values. Use euclidean distance to downsample from float coordinates.
    void addfXY(float xF, float yF, float r, float g, float b, float w) {

      //      Serial.printf("Add (%f, %f) to (r: %f, g: %f, b: %f)", xF, yF, r, g, b);

      // Take the centre of the interpolation as the floor of the floats.
      int xC = floor(xF);
      int yC = floor(yF);
      //      Serial.printf("Centre: (%d, %d)\n", xC, yC);

      for (int x = xC - DOWNSAMPLE_WINDOW; x <= xC + DOWNSAMPLE_WINDOW; x++) {
        if ((x < 0) || (x >= MAX_X)) {
          continue;
        }
        float xx = (float)x;

        for (int y = yC - DOWNSAMPLE_WINDOW; y <= yC + DOWNSAMPLE_WINDOW; y++) {
          if ((y < 0) || (y >= MAX_Y)) {
            continue;
          }
          float yy = (float)y;

          float distance = sqrt(sq(xF - (float)x) + sq(yF - (float)y));

          //          Multiply.
          //          float multiplier = 1.0f - distance;
          //          float rr = r * multiplier;
          //          float gg = g * multiplier;
          //          float bb = b * multiplier;

          // vs Subtract. This allows for values greater than 1.0 to give greater radius.
          float rr = r - distance;
          float gg = g - distance;
          float bb = b - distance;
          float ww = w - distance;

          //          Serial.printf("(%2d, %2d) = %2f => (%2f, %2f, %2f)   ", x, y, distance, rr, gg, bb);

          add(x, y, rr, gg, bb, ww);
        }
        //        Serial.println("");
      }
    }

    // decay each pixel by this proportion
    void decay(float amount) {
      float mult = 1.0 - amount;
      for (int x = 0; x < MAX_X; x++) {
        for (int y = 0; y < MAX_Y; y++) {
          this->rBuf[x][y] *= mult;
          this->gBuf[x][y] *= mult;
          this->bBuf[x][y] *= mult;
          this->wBuf[x][y] *= mult;
        }
      }
    }

    // Set pixel.
    void set(int x, int y, float rBrightness, float gBrightness, float bBrightness, float wBrightness) {
      if (x > MAX_X) {
        return;
      }

      if (y > MAX_Y) {
        return;
      }

      rBuf[x][y] = min(1.0f, rBrightness);
      gBuf[x][y] = min(1.0f, gBrightness);
      bBuf[x][y] = min(1.0f, bBrightness);
      wBuf[x][y] = min(1.0f, wBrightness);
    }

    void clear() {
      for (int x = 0; x < MAX_X; x++) {
        for (int y = 0; y < MAX_Y; y++) {
          this->set(x, y, 0.0f, 0.0f, 0.0f, 0.0f);
        }
      }
    }

    void display() {
      //      Serial.printf("Display\n");
      for (int x = 0; x < MAX_X; x++) {
        for (int y = 0; y < MAX_Y; y++) {
          float w = wBuf[x][y];
          float r = rBuf[x][y];
          float g = gBuf[x][y];
          float b = bBuf[x][y];

          // Also project rainbow into RGB.
          // Add these on top of the existing values. They may saturate, but that's taken care of below.
          // no idea what this looks like as it's going into WWA pixels!
          if (w < 0.5) {
            // less than half brightness maps to cold tail.
            r += w * 0.1;
            g += w;
            b += w * 0.5;
          } else if (w < 0.7) {
            // Decaying to blue/warm
            r += w * 0.5;
            g += 0.0f;
            b += w;
          } else {
            // Full brightness maps to red/amber.
            r += w;
            g += 0.0f;
            b += w;
          }

          int position = POSITION_LUT[y][x];
          STRAND.pixels[position] = pixelFromRGB(
                                      min((int)(r * MAX_INTENSITY), 255),
                                      min((int)(g * MAX_INTENSITY), 255),
                                      min((int)(b * MAX_INTENSITY), 255));

        }
      }


      strand_t * strand = &STRAND;
      digitalLeds_drawPixels(&strand, 1);
    }
};
class Snowflake {
  public:

    bool active;
    // From 0 to MAX_X.
    float x;

    // From 0 to MAX_Y.
    float y;

    float r;
    float g;
    float b;

    float velocityX;
    float velocityY;

    void start(float x) {

      this->y = 0;
      this->x = x;
      this->velocityY = ((float)random(100) / 100.0f) * (MAX_Y_VELOCITY - MIN_Y_VELOCITY) + MIN_Y_VELOCITY ;

      // Warm and cold white, but no amber.
      int colour = random(2);
      if (colour == 0) {
        this->r = 0.0f;
        this->g = 1.0f;
        this->b = 0.0f;
      } else if (colour == 1) {
        this->r = 0.0f;
        this->g = 0.0f;
        this->b = 1.0f;
      }

      //Serial.printf("MIN: %f, MAX: %f, DIFF: %f \n", MIN_Y_VELOCITY, MAX_Y_VELOCITY, (MAX_Y_VELOCITY - MIN_Y_VELOCITY));
      active = true;
    }

    void tick() {
      if (active) {
        //Serial.printf("Tick snowflake at %f...\n", this->x);
        this->x += this->velocityX;
        this->y += this->velocityY;

        // Stop being active when it gets to the bottom.
        if (this->y > (float)MAX_Y) {
          //Serial.printf("Stop snowflake at %f...\n", this->x);
          active = false;
        }
      }
    }

    void init() {
      this->active = false;
      this->velocityX = 0.0f;
      this->velocityY = 0.05f;
    }

    void paint(Picture *picture) {
      if (this->active) {

        //picture->add(floor(x), floor(y), 0.0f, 0.0f, 1.0f);
        picture->addfXY(this->x, this->y, this->r, this->g, this->b, 0);
      }
    }
};

class Meteor {
  public:

    bool active;
    // From 0 to MAX_X.
    float x;

    // From 0 to MAX_Y.
    float y;

    float velocityX;
    float velocityY;

    void start(float y) {

      this->y = y;
      // currently only right-to-left
      this->x = MAX_X;
      this->velocityX = 0 - (((float)random(100) / 100.0f) * (MAX_X_VELOCITY - MIN_X_VELOCITY) + MIN_X_VELOCITY) ;
      //Serial.printf("MIN: %f, MAX: %f, DIFF: %f \n", MIN_X_VELOCITY, MAX_X_VELOCITY, (MAX_X_VELOCITY - MIN_X_VELOCITY));
      Serial.printf("Start meteor! At: (%f, %f) velocity (%f, %f)...\n", x, y, velocityX, velocityY);
      active = true;
    }

    void tick() {
      //            Serial.printf("Tick meteor! Active %d At: (%f, %f) velocity %f...\n",active, x, y, velocityY);

      if (active) {
        //Serial.printf("Tick snowflake at %f...\n", this->x);
        this->x += this->velocityX;
        this->y += this->velocityY;

        // Stop being active when it gets to the bottom.
        if (this->x < (float)0.0f) {
          Serial.printf("Stop meteor at %f...\n", this->x);
          active = false;
        }
      }
    }

    void init() {
      this->active = false;
      this->velocityX = 0.0f;
      this->velocityY = 0.0f;
    }

    void paint(Picture *picture) {
      if (this->active) {
        picture->addfXY(x, y, 1.0f, 0.0f, 0.0f, 0.0f);
      }
    }
};

class Snowflakes {
    Snowflake snowflakes[MAX_SNOWFLAKES];

  public:

    void init() {
      Serial.printf("Init snowflakes...\n");
      for (int i = 0; i < MAX_SNOWFLAKES; i++) {
        snowflakes[i].init();
      }
    }

    void paint(Picture *picture) {
      //Serial.printf("Paint snowflakes...\n");
      for (int i = 0; i < MAX_SNOWFLAKES; i++) {
        snowflakes[i].paint(picture);
      }


    }

    void tick() {
      //Serial.printf("Tick snowflakes...\n");
      for (int i = 0; i < MAX_SNOWFLAKES; i++) {
        snowflakes[i].tick();
      }
    }

    // Start the first (if any) available snowflake at the given x position.
    void start(float x) {
      Serial.printf("Start snowflake at %f...\n", x);
      for (int i = 0; i < MAX_SNOWFLAKES; i++) {
        if (!snowflakes[i].active) {
          snowflakes[i].start(x);
          break;
        }
      }
    }
};


class Snow {
  public:
    Picture picture;
    Snowflakes snowflakes;
    Meteor meteor;

    // Tick count.
    long tickCount;

    void init() {

      tickCount = 0;

      snowflakes.init();
      meteor.init();
      picture.init();
      picture.display();
    }


    void paint() {

      picture.clear();
      snowflakes.paint(&picture);
      meteor.paint(&picture);
      picture.display();

    }

    void tick() {
      tickCount++;


      if (tickCount % 10 == 0) {
        // How many subdivisions per integer X should be used for snowflake starting positions?
        // For now, just keep them to the precise column, no float.
        const int RESOLUTION = 1;
        float col = ((float)random(0, MAX_X * RESOLUTION)) / (float)RESOLUTION;
        snowflakes.start(col);
      }

      if (tickCount % 2000 == 0) {
        if (!meteor.active) {
          int row = random(0, MAX_Y / 2);
          Serial.printf("Meteor row %d...\n", row);

          meteor.start((float) row);
        }
      }

      snowflakes.tick();
      meteor.tick();
      paint();
    }
};

Snow snow;

class Lissajous {
    float count = 0;
    Picture picture;


  public:
    void init() {
      picture.init();
    }

    void tick() {
      count += 0.01;

      // lower frequency from 0.0 to 2.0 modulating the X axis (it has more detail).
      float lfox =  (1.0 + sin(count / 10.0)) * 0.25;
      float lfoy =  (1.0 + sin(count / 20.0)) * 0.25;

      Serial.printf("LFO: (%f, %f)\n", lfox, lfoy);

      float x = (sin(count * lfox) * (float)MAX_X / 2.5f) + (float)MAX_X / 2.0f;
      float y = (cos(count * lfoy) * (float)MAX_Y / 2.5f) + (float)MAX_Y / 2.0f;

      //Serial.printf("t: %d = (%f, %f) \n", count, x, y);

      //picture.clear();
      picture.addfXY(x, y, 0.0f, 0.0f, 0.0f, 1.0f);
      picture.display();
      picture.decay(0.1);
    }
};

Lissajous lissajous;


/*
   Setup
*/
bool initStrands()
{
  digitalLeds_initDriver();
  gpioSetup(STRAND.gpioNum, OUTPUT, LOW);

  strand_t * strand = &STRAND;
  int rc = digitalLeds_addStrands(&strand, 1);
  if (rc) {
    Serial.print("Init rc = ");
    Serial.println(rc);
    return false;
  }
  return true;
}

// Set a pixel in the strand using the podition LUT.
void set(int x, int y, float r, float g, float b) {
  if (x > MAX_X) {
    return;
  }

  if (y > MAX_Y) {
    return;
  }

  int position = POSITION_LUT[y][x];

  STRAND.pixels[position] = pixelFromRGB(
                              (int)(r * MAX_INTENSITY),
                              (int)(g * MAX_INTENSITY),
                              (int)(b * MAX_INTENSITY));
}


/*
   Demos
*/

void demoFade() {
  for (int x = 0; x < MAX_X; x++) {
    for (int y = 0; y < MAX_Y; y++) {
      set(x,
          y,
          (float)x / (float)MAX_X,
          (float)y / (float)MAX_Y,
          0);
    }
  }

  strand_t * strand = &STRAND;
  digitalLeds_drawPixels(&strand, 1);
}

void setup()
{
  Serial.begin(115200);
  Serial.println("Initializing...");

  if (!initStrands()) {
    Serial.println("Init FAILURE: halting");
    while (true) {
      delay(100);
    }
  }
  Serial.println("Pause...");
  delay(5000);
  Serial.println("Init complete");

  //  demoChase();

  snow.init();
  lissajous.init();
}



void loop()
{
  //snow.tick();
  lissajous.tick();
  delay(10);
}
